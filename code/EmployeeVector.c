#include "EmployeeVector.h"
#include <stdio.h>
#include <stdlib.h>

int EmployeeVector__find(struct EmployeeVector* self, char* searchName) {
    for(int i=0; i < self->size; i++)
        if(compare_strings(self->list[i]->username, searchName))
            return i;
    return -1;
}

void EmployeeVector__extend(struct EmployeeVector* self) {
    struct EmployeeVector** newVector;

    newVector = realloc(self->list, 2*self->totalLength*sizeof(struct EmployeeVector*));
    if(newVector == NULL) {
        newVector = (struct Employee**)malloc(2*self->totalLength*sizeof(struct EmployeeVector*));
        for(int i=0; i<self->size; i++)
            newVector[i] = self->list[i];
        free(self->list);
    }
    
    self->list = newVector;
    self->totalLength *= 2;
}

void EmployeeVector__push(struct EmployeeVector* self, struct Employee* newEmployee) {
    if(self->totalLength <= self->size)
        EmployeeVector__extend(self);
    self->list[self->size] = newEmployee;
    self->size++;
}

void EmployeeVector__printList(struct EmployeeVector* self) {
    printf("____________________________\nEmployees\n____________________________\n");
    for(int i=0; i<self->size; i++)
        printf("%d | %s\n", i+1, self->list[i]->username);
    printf("____________________________\n");
}

void EmployeeVector__free(struct EmployeeVector* self) {
    for(int i=0; i<self->size; i++)
        self->list[i]->free(self->list[i]);
    free(self->list);
    free(self);
}

void EmployeeVector__remove(struct EmployeeVector* self, int index) {
    self->list[index]->free(self->list[index]);
    self->list[index] = self->list[self->size-1];
    self->list[self->size-1] = NULL;
    self->size--;
}

void EmployeeVector__saveInFile(struct EmployeeVector* self, FILE* file) {
    fprintf(file, "%d\n", self->size);
    for(int i=0; i<self->size; i++)
        self->list[i]->saveInFile(self->list[i], file);
}

void EmployeeVector__loadOfFile(struct EmployeeVector* self, FILE* file) {
    int size;
    fscanf(file, "%d\n", &size);
    for(int i=0; i<size; i++) {
        char* username = (char *)malloc(100);
        char* password = (char *)malloc(100);
        fscanf(file, "%s %s\n", username, password);
        self->push(self, newEmployee(username, password));
    }
}


struct EmployeeVector* newEmployeeVector() {
    struct EmployeeVector* e = (struct EmployeeVector*)malloc(sizeof(struct EmployeeVector));
    e->push = EmployeeVector__push;
    e->find = EmployeeVector__find;
    e->printList = EmployeeVector__printList;
    e->free = EmployeeVector__free;
    e->remove = EmployeeVector__remove;
    e->size = 0;
    e->totalLength = 1;
    e->list = (struct Employee**)malloc(e->totalLength*sizeof(struct Employee*));
    e->saveInFile = EmployeeVector__saveInFile;
    e->loadOfFile = EmployeeVector__loadOfFile;
    return e;
}