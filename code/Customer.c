#include "Customer.h"
#include <stdlib.h>
#include "Exeption.h"

#define True 1
#define False 0

void Customer__calculate_profit(struct Customer* self) {
    if(self->quantity > 0) self->quantity *= 1.18;
}

struct TurnOver* newTurnOver(struct TurnOver* prev, char event, long int change) {
    struct TurnOver* t = (struct TurnOver*)malloc(sizeof(struct TurnOver));
    t->prev = prev;
    t->index = prev->index+1;
    t->event = event;
    t->changeQuantity = change;
    t->quantity = prev->quantity+change;
    return t;
}

struct TurnOver* loadTurnOver(int index, char event, long int change, long int quantity) {
    struct TurnOver* t = (struct TurnOver*)malloc(sizeof(struct TurnOver));
    t->prev = NULL;
    t->index = index;
    t->event = event;
    t->changeQuantity = change;
    t->quantity = quantity;
    return t;
}

struct TurnOver* firstTurnOver(long int quantity) {
    struct TurnOver* t = (struct TurnOver*)malloc(sizeof(struct TurnOver));
    t->prev = NULL;
    t->index = 0;
    t->event = 'S';
    t->changeQuantity = quantity;
    t->quantity = quantity;
    return t;
}

void Customer__increase(struct Customer* self, long int quantity) {
    self->quantity += quantity;
    self->turnOver = newTurnOver(self->turnOver, 'I', quantity);
}

int Customer__decrease(struct Customer* self, long int quantity) {
    if(self->quantity - quantity < -10000) {
        notEnoughMoney();
        return False;
    }
    self->quantity -= quantity;
    self->turnOver = newTurnOver(self->turnOver, 'D', -1*quantity);
    return True;
}

void Customer__reportTurnOver(struct Customer* self) {
    struct TurnOver* turnover = self->turnOver;
    printf("____________________________\n");
    for(int i=0; i<4; i++) {
        printf("%d : %c (%ld) -> %ld\n",
            turnover->index, turnover->event, turnover->changeQuantity, turnover->quantity); 
        turnover = turnover->prev;
        if(turnover == NULL) break;
    }
    printf("____________________________\n");
}

void Customer__free(struct Customer* self) {
    free(self->username);
    free(self->password);
    free(self->name);
    free(self->familyname);
    struct TurnOver* turnover = self->turnOver;
    while(turnover != NULL) {
        struct TurnOver* prev = turnover->prev;
        free(turnover);
        turnover = prev;
    }
    free(self);
}

void Customer__saveInFile(struct Customer* self, FILE* file) {
    fprintf(file, "%s %s %s %s %ld %ld %d\n",
        self->username, self->password, self->name, self->familyname, self->quantity, self->accountnumber,
        self->turnOver->index+1);
    struct TurnOver* turnover = self->turnOver;
    while(turnover != NULL) {
        fprintf(file, "%d %c %ld %ld\n",
            turnover->index, turnover->event, turnover->changeQuantity, turnover->quantity);
        turnover = turnover->prev;
    }
}

struct Customer* newCustomer(char* un, char* pass, char* n, char* fn, long int q) {
    struct Customer* c = (struct Customer*)malloc(sizeof(struct Customer));
    c->username = un;
    c->password = pass;
    c->name = n;
    c->familyname = fn;
    c->quantity = q;
    c->accountnumber = rand()%1000000;
    c->free = Customer__free;
    c->calculate_profit = Customer__calculate_profit;
    c->increase = Customer__increase;
    c->decrease = Customer__decrease;
    c->turnOver = firstTurnOver(q);
    c->reportTurnOver = Customer__reportTurnOver;
    c->saveInFile = Customer__saveInFile;
    return c;
}

struct Customer* loadCustomer(char* un, char* pass, char* n, char* fn, long int q, long int ac, struct TurnOver* turnOver) {
    struct Customer* c = (struct Customer*)malloc(sizeof(struct Customer));
    c->username = un;
    c->password = pass;
    c->name = n;
    c->familyname = fn;
    c->quantity = q;
    c->accountnumber = ac;
    c->free = Customer__free;
    c->calculate_profit = Customer__calculate_profit;
    c->increase = Customer__increase;
    c->decrease = Customer__decrease;
    c->turnOver = turnOver;
    c->reportTurnOver = Customer__reportTurnOver;
    c->saveInFile = Customer__saveInFile;
    return c;
}