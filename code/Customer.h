#ifndef CUSTOMER
#define CUSTOMER

#include <stdio.h>

struct TurnOver {
    int index;
    char event;
    long int changeQuantity;
    long int quantity;
    struct TurnOver* prev;
};

struct TurnOver* newTurnOver(struct TurnOver*, char, long int);
struct TurnOver* loadTurnOver(int, char, long int, long int);


struct Customer {
    char* username;
    char* password;
    char* name;
    char* familyname;
    long int quantity;
    long int accountnumber;
    struct TurnOver* turnOver;
    void (*free)(struct Customer*);
    void (*calculate_profit)(struct Customer*);
    void (*increase)(struct Customer*, long int);
    int (*decrease)(struct Customer*, long int);
    void (*reportTurnOver)(struct Customer*);
    void (*saveInFile)(struct Customer*, FILE*);
};

struct Customer* newCustomer(char*, char*, char*, char*, long int);
struct Customer* loadCustomer(char*, char*, char*, char*, long int, long int, struct TurnOver*);

#endif