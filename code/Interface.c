#include "Interface.h"
#include <stdio.h>
#include <stdlib.h>

#define True 1
#define False 0
#define Save "save"
#define Signup_Employee "signup_employee"
#define Signup_Customer "signup_customer"
#define Login_Employee "login_employee"
#define Login_Customer "login_customer"
#define Logout "logout"
#define Employee_List "employee_list"
#define Customer_List "customer_list"
#define Delete_User "del_user"
#define Calculate_Profit "calculate_profit"
#define Show_Credit "show_credit"
#define Increase "increase"
#define Decrease "decrease"
#define Transfer "transfer_from"
#define Worth "banck_worth"
#define Report_TurnOver "report_turnover"


int checkLogin(int userIndex, int exeption) {
    if(userIndex < 0) {
        if(exeption) notLogin();
        return False;
    }
    return True;
}

int checkLogout(int userIndex, int exeption) {
    if(userIndex >= 0) {
        if(exeption) notLogout();
        return False;
    }
    return True;
}

int checkEmployeeAccess(int employeeAccess, int exeption) {
    if(!employeeAccess) {
        if(exeption) accessDenied();
        return False;
    }
    return True;
}

char* getWord(FILE* commandLine) {
    char* word = (char *)malloc(30);
    fscanf(commandLine, "%s", word);
    if(compare_strings(word, "")) {
        WrongCommand();
        free(word);
        return NULL;
    }
    return word;
}

long int getLongInt(FILE* commandLine) {
    long int longInt;
    fscanf(commandLine, "%ld", &longInt);
    if(longInt == NULL) {
        WrongCommand();
        return NULL;
    }
    return longInt;
}

void Interface__signup_employee(struct Interface* self, FILE* commandLine) {
    if(self->core->employeeList->size > 0){
        if(!checkLogin(self->userIndex, False)) {
        loginToAdd();
        return;
        }
        if(!checkEmployeeAccess(self->employeeAccess, True)) return;
    }

    char* username = getWord(commandLine);
    if(username == NULL) return;
    char* password = getWord(commandLine);
    if(password == NULL) {
        free(username);
        return;
    }
    
    self->core->signup_employee(self->core, username, password);
    username = NULL;
    password = NULL;
}

char* Interface__getInfoUnit(char* key, int needResult) {
    char* result;
    char* infoString = (char *)malloc(100);
    char* infoKey;
    FILE* infoLine;

    int next = False;
    while(!next) {
        printf("> ");
        fgets(infoString,100,stdin);
        infoLine = fmemopen(infoString, 100, "r");        
        infoKey = getWord(infoLine);
        if(compare_strings(infoKey, key)) {
            next = True;
            if(needResult) {
                result = getWord(infoLine);
                if(result == NULL)
                    next = False;
            }
        } else if(infoKey != NULL) {
            printf("You didn't complete customer's information, complete it first!.\n");
        }
        fclose(infoLine);
        free(infoKey);
    }
    free(infoString);
    return result;
}

long int Interface__getInfoLongIntUnit(char* key, char* username, int needResult) {
    long int result;
    char* infoString = (char *)malloc(100);
    char* infoKey;
    FILE* infoLine;

    int next = False;
    while(!next) {
        printf("> ");
        fgets(infoString,100,stdin);
        infoLine = fmemopen(infoString, 100, "r");        
        infoKey = getWord(infoLine);
        if(compare_strings(infoKey, key)) {
            next = True;

            char* inputUsername = getWord(infoLine);
            if(!compare_strings(username, inputUsername))
                next = False;
            else
                result = getLongInt(infoLine);
            if(result == NULL && needResult) 
                next = False;
            free(inputUsername);

        } else if(infoKey != NULL) {
            printf("You didn't complete customer's information, complete it first!.\n");
        }
        fclose(infoLine);
        free(infoKey);
    }
    free(infoString);

    return result;
}

struct CustomerInfo {
    char* name;
    char* familyname;
    long int quantity;
};

struct CustomerInfo* Interface__getCustomerInfo(char* username) {
    struct CustomerInfo* info = (struct CustomerInfo*)malloc(sizeof(struct CustomerInfo));
    
    printf("Please enter information :\n");
    info->name = Interface__getInfoUnit("register_name", True);
    info->familyname = Interface__getInfoUnit("register_familyname", True);
    info->quantity = Interface__getInfoLongIntUnit("increase", username, True);
    Interface__getInfoUnit("assign_accountnumber", False);
    printf("Info catched successfully.\n");
    
    return info;
}

void Interface__signup_customer(struct Interface* self, FILE* commandLine) {
    if(!checkLogin(self->userIndex, False)) {
        loginToAdd();
        return;
    }
    if(!checkEmployeeAccess(self->employeeAccess, True)) return;

    char* username = getWord(commandLine);
    if(username == NULL) return;
    char* password = getWord(commandLine);
    if(password == NULL) {
        free(username);
        return;
    }
    if(self->core->checkExist(self->core, username, True)) {
        free(username);
        free(password);
        return;
    }
    printf("Customer %s successfully added.\n", username);
    struct CustomerInfo* info = Interface__getCustomerInfo(username);
    self->core->signup_customer(self->core, username, password, info->name, info->familyname, info->quantity);
    username = NULL;
    password = NULL;
    info->name = NULL;
    info->familyname = NULL;
    free(info);
}

void Interface__login_employee(struct Interface* self, FILE* commandLine) {
    if(!checkLogout(self->userIndex, True)) return;

    char* username = getWord(commandLine);
    if(username == NULL) return;
    char* password = getWord(commandLine);
    if(password == NULL) {
        free(username);
        return;
    }

    self->userIndex = self->core->login_employee(self->core, username, password);
    if(self->userIndex >= 0) self->employeeAccess = True;
    free(username);
    free(password);
    if(self->userIndex >= 0) printf("Logged in!\n");
}

void Interface__login_customer(struct Interface* self, FILE* commandLine) {
    if(!checkLogout(self->userIndex, True)) return;

    char* username = getWord(commandLine);
    if(username == NULL) return;
    char* password = getWord(commandLine);
    if(password == NULL) {
        free(username);
        return;
    }
    
    self->userIndex = self->core->login_customer(self->core, username, password);
    free(username);
    free(password);
    if(self->userIndex >= 0) printf("Logged in!\n");
}

void Interface__logout(struct Interface* self) {
    if(!checkLogin(self->userIndex, True)) return;
    self->userIndex = -1;
    self->employeeAccess = False;
    printf("Logged out!\n");
}

void userDeclare(struct Interface* self) {
    if(!checkLogin(self->userIndex, False)) {
        printf("(?) >>> ");
    } else if(checkEmployeeAccess(self->employeeAccess, False)) {
        printf("Employee %s >>> ", self->core->employeeList->list[self->userIndex]->username);
    } else {
        printf("Customer %s >>> ", self->core->customerList->list[self->userIndex]->username);
    }
}

void Interface__employee_list(struct Interface* self) {
    if(!checkLogin(self->userIndex, True)) return;
    if(!checkEmployeeAccess(self->employeeAccess, True)) return;
    self->core->employee_list(self->core);
}

void Interface__customer_list(struct Interface* self) {
    if(!checkLogin(self->userIndex, True)) return;
    if(!checkEmployeeAccess(self->employeeAccess, True)) return;
    self->core->customer_list(self->core);
}

void Interface__delete_user(struct Interface* self, FILE* commandLine) {
    if(!checkLogin(self->userIndex, True)) return;
    if(!checkEmployeeAccess(self->employeeAccess, True)) return;

    char* username = getWord(commandLine);
    if(username == NULL) return;
    
    char* runtimeUsername = self->core->employeeList->list[self->userIndex]->username;
    char* runtimePassword = self->core->employeeList->list[self->userIndex]->password;
    if(compare_strings(runtimeUsername, username))
        Interface__logout(self);
    else
        self->userIndex = self->core->login_employee(self->core, runtimeUsername, runtimePassword);
    
    self->core->delete_user(self->core, username);
    free(username);
    runtimeUsername = NULL;
    runtimePassword = NULL;
}

void Interface__calculate_profit(struct Interface* self) {
    if(!checkLogin(self->userIndex, True)) return;
    
    self->core->calculate_profit(self->core);
    printf("Done.\n");
}

void Interface__show_credit(struct Interface* self, FILE* commandLine) {
    if(!checkLogin(self->userIndex, True)) return;
    
    char* username = getWord(commandLine);
    if(username == NULL) return;

    if(!checkEmployeeAccess(self->employeeAccess, False) &&
    !compare_strings(username, self->core->customerList->list[self->userIndex]->username)) {
        accessDenied();
        free(username);
        return;
    }
    self->core->show_credit(self->core, username);
    free(username);
}

void Interface__increase(struct Interface* self, FILE* commandLine) {
    if(!checkLogin(self->userIndex, True)) return;
    if(checkEmployeeAccess(self->employeeAccess, False)) {
        accessDenied();
        return;
    }
    char* username = getWord(commandLine);
    if(username == NULL) return;
    if(!compare_strings(username, self->core->customerList->list[self->userIndex]->username)) {
        accessDenied();
        free(username);
        return;
    }
    long int quantity = getLongInt(commandLine);
    if(quantity == NULL) {
        free(username);
        return;
    }
    self->core->increase(self->core, self->userIndex, quantity);
    free(username);
    printf("Done!\n");
}

void Interface__decrease(struct Interface* self, FILE* commandLine) {
    if(!checkLogin(self->userIndex, True)) return;
    if(checkEmployeeAccess(self->employeeAccess, False)) {
        accessDenied();
        return;
    }
    char* username = getWord(commandLine);
    if(username == NULL) return;
    if(!compare_strings(username, self->core->customerList->list[self->userIndex]->username)) {
        accessDenied();
        free(username);
        return;
    }
    long int quantity = getLongInt(commandLine);
    if(quantity == NULL) {
        free(username);
        return;
    }
    if(self->core->decrease(self->core, self->userIndex, quantity))
        printf("Done!\n");
    free(username);
}

void Interface__transfer(struct Interface* self, FILE* commandLine) {
    if(!checkLogin(self->userIndex, True)) return;
    if(checkEmployeeAccess(self->employeeAccess, False)) {
        accessDenied();
        return;
    }
    char* username = getWord(commandLine);
    if(username == NULL) return;
    if(!compare_strings(username, self->core->customerList->list[self->userIndex]->username)) {
        accessDenied();
        free(username);
        return;
    }
    long int quantity = getLongInt(commandLine);
    if(quantity == NULL) {
        free(username);
        return;
    }
    char* to = getWord(commandLine);
    if(to == NULL) {
        free(username);
        return;
    }
    if(!compare_strings(to, "to")) {
        WrongCommand();
        free(username);
        free(to);
        return;
    }
    char* target = getWord(commandLine);
    if(target == NULL) {
        free(username);
        free(to);
        return;
    }

    if(self->core->transfer(self->core, self->userIndex, quantity, target))
        printf("Done!\n");
    free(username);
    free(to);
    free(target);
}

void Interface__worth(struct Interface* self) {
    if(!checkLogin(self->userIndex, True)) return;
    if(!checkEmployeeAccess(self->employeeAccess, True)) return;

    self->core->showWorth(self->core);
}

void Interface__reportTurnOver(struct Interface* self, FILE* commandLine) {
    if(!checkLogin(self->userIndex, True)) return;
    
    char* username = getWord(commandLine);
    if(username == NULL) return;

    if(!checkEmployeeAccess(self->employeeAccess, False) &&
    !compare_strings(username, self->core->customerList->list[self->userIndex]->username)) {
        accessDenied();
        free(username);
        return;
    }
    self->core->reportTurnOver(self->core, username);
    free(username);
}

int Interface__exit(FILE* commandLine) {
    char* key;
    key = getWord(commandLine);
    if(key == NULL)
        return False;
    if(!(compare_strings(key, "and"))) {
        free(key);
        return False;
    }
    free(key);
    key = getWord(commandLine);
    if(key == NULL)
        return False;
    if(!(compare_strings(key, "exit"))) {
        free(key);
        return False;
    }
    free(key);
    return True;
}


void Interface__run(struct Interface* self) {
    char* commandString = (char *)malloc(100);
    FILE* commandLine;
    char* key;

    printf("Welcome >>> ");
    while(fgets(commandString,100,stdin)) {
        commandLine = fmemopen(commandString, 100, "r");
        key = getWord(commandLine);
        // printf("\n");
        
        if(compare_strings(key, Save) && Interface__exit(commandLine)) {
            free(key);
            break; 
        } else if(compare_strings(key, Signup_Employee))
            Interface__signup_employee(self, commandLine);
        else if(compare_strings(key, Signup_Customer))
            Interface__signup_customer(self, commandLine);
        else if(compare_strings(key, Login_Employee))
            Interface__login_employee(self, commandLine);
        else if(compare_strings(key, Login_Customer))
            Interface__login_customer(self, commandLine);
        else if(compare_strings(key, Logout))
            Interface__logout(self);
        else if(compare_strings(key, Employee_List))
            Interface__employee_list(self);
        else if(compare_strings(key, Customer_List))
            Interface__customer_list(self);
        else if(compare_strings(key, Delete_User))
            Interface__delete_user(self, commandLine);
        else if(compare_strings(key, Calculate_Profit))
            Interface__calculate_profit(self);
        else if(compare_strings(key, Show_Credit))
            Interface__show_credit(self, commandLine);
        else if(compare_strings(key, Increase))
            Interface__increase(self, commandLine);
        else if(compare_strings(key, Decrease))
            Interface__decrease(self, commandLine);
        else if(compare_strings(key, Transfer))
            Interface__transfer(self, commandLine);
        else if(compare_strings(key, Worth))
            Interface__worth(self);
        else if(compare_strings(key, Report_TurnOver))
            Interface__reportTurnOver(self, commandLine);
        else unknownComman();

        free(key);
        fclose(commandLine);
        userDeclare(self);
    }
    free(commandString);
}

void Interface__free(struct Interface* self) {
    self->core->free(self->core);
    free(self);
}


struct Interface* newInterface(struct Core* c) {
    struct Interface* i = (struct Interface*)malloc(sizeof(struct Interface));
    i->core = c;
    i->run = Interface__run;
    i->free = Interface__free;
    i->userIndex = -1;
    i->employeeAccess = False;
    return i;
}