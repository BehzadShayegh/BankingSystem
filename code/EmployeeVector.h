#ifndef EMPLOYEEVECTOR
#define EMPLOYEEVECTOR

#include "Employee.h"
#include "myString.h"

struct EmployeeVector {
    struct Employee** list;
    int size;
    int totalLength;

    void (*push)(struct EmployeeVector*, struct Employee*);
    int (*find)(struct EmployeeVector*, char*);
    void (*printList)(struct EmployeeVector*);
    void (*free)(struct EmployeeVector*);
    void (*remove)(struct EmployeeVector*, int);
    void (*saveInFile)(struct EmployeeVector*, FILE*);
    void (*loadOfFile)(struct EmployeeVector*, FILE*);
};

struct EmployeeVector* newEmployeeVector();

#endif