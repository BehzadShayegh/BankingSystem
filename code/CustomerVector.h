#ifndef CUSTOMERVECTOR
#define CUSTOMERVECTOR

#include "Customer.h"
#include "myString.h"

struct CustomerVector {
    struct Customer** list;
    int size;
    int totalLength;

    long int (*push)(struct CustomerVector*, struct Customer*);
    int (*find)(struct CustomerVector*, char*);
    void (*printList)(struct CustomerVector*);
    void (*free)(struct CustomerVector*);
    void (*remove)(struct CustomerVector*, int);
    void (*calculate_profit)(struct CustomerVector*);
    long int (*sumQuantity)(struct CustomerVector*);
    void (*saveInFile)(struct CustomerVector*, FILE*);
    void (*loadOfFile)(struct CustomerVector*, FILE*);
};

struct CustomerVector* newCustomerVector();

#endif