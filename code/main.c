#include "Core.h"
#include "Interface.h"

int main(){
	// srand(time (0));

    struct Core* core = newCore();
	core->load_data(core, "./data.txt");
	struct Interface* interface = newInterface(core);
	
	interface->run(interface);
	
	core->save_data(core, "./data.txt");
	interface->free(interface);
	
	return 0;
}
