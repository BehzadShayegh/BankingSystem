#include "myString.h"
#include <stdio.h>

#define True 1
#define False 0

int compare_strings(char* a, char* b)
{
   if(a == NULL || b == NULL)
      return False;
      
   int c = 0;
 
   while (a[c] == b[c]) {
      if (a[c] == '\0' || b[c] == '\0')
         break;
      c++;
   }
   
   if (a[c] == '\0' && b[c] == '\0')
      return True;
   else
      return False;
}