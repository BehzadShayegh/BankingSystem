#ifndef EMPLOYEE
#define EMPLOYEE

#include <stdio.h>

struct Employee {
    char* username;
    char* password;
    void (*free)(struct Employee*);
    void (*saveInFile)(struct Employee*, FILE*);
};

struct Employee* newEmployee(char*, char*);

#endif