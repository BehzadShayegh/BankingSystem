#ifndef CORE
#define CORE

#include "EmployeeVector.h"
#include "Employee.h"
#include "Exeption.h"
#include "myString.h"
#include "Customer.h"
#include "CustomerVector.h"

struct Core {
    struct EmployeeVector* employeeList;
    struct CustomerVector* customerList;
    void (*signup_employee)(struct Core*, char*, char*);
    void (*signup_customer)(struct Core*, char*, char*, char*, char*, long int);
    void (*free)(struct Core*);
    int (*login_employee)(struct Core*, char*, char*);
    void (*employee_list)(struct Core*);
    int (*login_customer)(struct Core*, char*, char*);
    void (*customer_list)(struct Core*);
    void (*delete_user)(struct Core*, char*);
    int (*checkExist)(struct Core*, char*, int);
    void (*calculate_profit)(struct Core*);
    void (*show_credit)(struct Core*, char*);
    void (*increase)(struct Core*, int, long int);
    int (*decrease)(struct Core*, int, long int);
    int (*transfer)(struct Core*, int, long int, char*);
    void (*showWorth)(struct Core*);
    void (*reportTurnOver)(struct Core*, char*);
    void (*save_data)(struct Core*, char*);
    void (*load_data)(struct Core*, char*);
};

struct Core* newCore();

#endif