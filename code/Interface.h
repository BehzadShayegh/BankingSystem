#ifndef INTERFACE
#define INTERFACE

#include "Core.h"
#include "myString.h"
#include "Exeption.h"

struct Interface {
    struct Core* core;
    void (*run)(struct Interface*);
    void (*free)(struct Interface*);
    int userIndex;
    int employeeAccess;
};

struct Interface* newInterface(struct Core*);

#endif