#include "Employee.h"
#include <stdlib.h>

void Employee__free(struct Employee* self) {
    free(self->username);
    free(self->password);
    free(self);
}

void Employee__saveInFile(struct Employee* self, FILE* file) {
    fprintf(file, "%s %s\n", self->username, self->password);
}

struct Employee* newEmployee(char* n, char* p) {
    struct Employee* e = (struct Employee*)malloc(sizeof(struct Employee));
    e->username = n;
    e->password = p;
    e->free = Employee__free;
    e->saveInFile = Employee__saveInFile;
    return e;
}