#include "CustomerVector.h"
#include <stdio.h>
#include <stdlib.h>

int CustomerVector__find(struct CustomerVector* self, char* searchName) {
    for(int i=0; i < self->size; i++)
        if(compare_strings(self->list[i]->username, searchName))
            return i;
    return -1;
}

void CustomerVector__extend(struct CustomerVector* self) {
    struct CustomerVector** newVector;

    newVector = realloc(self->list, 2*self->totalLength*sizeof(struct CustomerVector*));
    
    if(newVector == NULL) {
        newVector = (struct Customer**)malloc(2*self->totalLength*sizeof(struct CustomerVector*));
        for(int i=0; i<self->size; i++)
            newVector[i] = self->list[i];
        free(self->list);
    }
    
    self->list = newVector;
    self->totalLength *= 2;
}

long int CustomerVector__push(struct CustomerVector* self, struct Customer* newCustomer) {
    if(self->totalLength <= self->size)
        CustomerVector__extend(self);
    self->list[self->size] = newCustomer;
    self->size++;
    return newCustomer->accountnumber;
}

void swap(struct Customer** list, int index1, int index2) {
    struct Customer* temp = list[index1];
    list[index1] = list[index2];
    list[index2] = temp;
    temp = NULL;
}

int findPoorest(struct Customer** list, int size) {
    int poorestIndex = 0;
    for(int index=1; index<size; index++)
        if(list[index]->quantity < list[poorestIndex]->quantity)
            poorestIndex = index;
    return poorestIndex;
}

void insertionSort(struct Customer** list, int size) {
    for(int i=size; i>0; i--)
        swap(list, i-1, findPoorest(list, i));
}

void CustomerVector__printList(struct CustomerVector* self) {
    insertionSort(self->list, self->size);
    printf("____________________________\nCustomers\tQuantities\n____________________________\n");
    for(int i=0; i<self->size; i++)
        printf("%d | %s   \t%ld\n", i+1, self->list[i]->username, self->list[i]->quantity);
    printf("____________________________\n");
}

void CustomerVector__free(struct CustomerVector* self) {
    for(int i=0; i<self->size; i++)
        self->list[i]->free(self->list[i]);
    free(self->list);
    free(self);
}

void CustomerVector__remove(struct CustomerVector* self, int index) {
    long int quantity = self->list[index]->quantity;
    if(quantity > 0) printf("Pay %ld to %s\n", quantity, self->list[index]->username);
    if(quantity < 0) printf("collect %ld from %s\n", -1*quantity, self->list[index]->username);
    self->list[index]->free(self->list[index]);
    self->list[index] = self->list[self->size-1];
    self->list[self->size-1] = NULL;
    self->size--;
}

void CustomerVector__calculate_profit(struct CustomerVector* self) {
    for(int i=0; i<self->size; i++)
        self->list[i]->calculate_profit(self->list[i]);
}

long int CustomerVector__sumQuantity(struct CustomerVector* self) {
    long int sum = 0;
    for(int i=0; i<self->size; i++)
        sum += self->list[i]->quantity;
    return sum;
}

void CustomerVector__saveInFile(struct CustomerVector* self, FILE* file) {
    fprintf(file, "%d\n", self->size);
    for(int i=0; i<self->size; i++)
        self->list[i]->saveInFile(self->list[i], file);
}

void CustomerVector__loadOfFile(struct CustomerVector* self, FILE* file) {
    int size;
    fscanf(file, "%d\n", &size);
    for(int i=0; i<size; i++) {
        char* username = (char *)malloc(100);
        char* password = (char *)malloc(100);
        char* name = (char *)malloc(100);
        char* familyname = (char *)malloc(100);
        long int quantity, accountnumber;
        int toNumber;
        fscanf(file, "%s %s %s %s %ld %ld %d\n",
            username, password, name, familyname, &quantity, &accountnumber, &toNumber);
        struct TurnOver *temp, *head, *next;
        for(int j=0; j<toNumber; j++) {
            int index;
            char event;
            long int change, quantity;
            fscanf(file, "%d %c %ld %ld\n",
                &index, &event, &change, &quantity);
            next = loadTurnOver(index, event, change, quantity);
            if(j==0) head = next;
            else temp->prev = next;
            temp = next;
        }
        self->push(self, loadCustomer(
            username, password, name, familyname, quantity, accountnumber, head));
    }
}


struct CustomerVector* newCustomerVector() {
    struct CustomerVector* e = (struct CustomerVector*)malloc(sizeof(struct CustomerVector));
    e->push = CustomerVector__push;
    e->find = CustomerVector__find;
    e->printList = CustomerVector__printList;
    e->free = CustomerVector__free;
    e->remove = CustomerVector__remove;
    e->size = 0;
    e->totalLength = 1;
    e->list = (struct Customer**)malloc(e->totalLength*sizeof(struct Customer*));
    e->calculate_profit = CustomerVector__calculate_profit;
    e->sumQuantity = CustomerVector__sumQuantity;
    e->saveInFile = CustomerVector__saveInFile;
    e->loadOfFile = CustomerVector__loadOfFile;
    return e;
}