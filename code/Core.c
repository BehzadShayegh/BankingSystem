#include "Core.h"
#include <stdio.h>
#include <stdlib.h>

#define True 1
#define False 0

void Core__signup_employee(struct Core* self, char* username, char* password) {
    if(self->checkExist(self, username, True)) {
        free(username);
        free(password);
        return;
    }
    self->employeeList->push(self->employeeList, newEmployee(username, password));
    printf("User %s successfully added!\n", username);
}

void Core__signup_customer(struct Core* self, char* username, char* password, char* name, char* familyname, long int quantity) {
    if(self->checkExist(self, username, True)) {
        free(username);
        free(password);
        free(name);
        free(familyname);
        return;
    }
    long int accountnumber = self->customerList->push(self->customerList, newCustomer(username, password, name, familyname, quantity));
    printf("Customer %s registered successfully with this information:\n", username);
    printf("%s\n%s\nCredit : %ld\nAccount number : %ld\n", name, familyname, quantity, accountnumber);
}

int Core__login_employee(struct Core* self, char* username, char* password) {
    int employeeIndex = self->employeeList->find(self->employeeList, username);
    if(employeeIndex < 0) {
        employeeNotExist();
        return -1;
    }
    if(!compare_strings(self->employeeList->list[employeeIndex]->password, password)) {
        wrongPassword();
        return -1;
    }
    return employeeIndex;
}

int Core__login_customer(struct Core* self, char* username, char* password) {
    int customerIndex = self->customerList->find(self->customerList, username);
    if(customerIndex < 0) {
        customerNotExist();
        return -1;
    }
    if(!compare_strings(self->customerList->list[customerIndex]->password, password)) {
        wrongPassword();
        return -1;
    }
    return customerIndex;
}

void Core__employee_list(struct Core* self) {
    self->employeeList->printList(self->employeeList);
}

void Core__customer_list(struct Core* self) {
    self->customerList->printList(self->customerList);
}

void Core__delete_user(struct Core* self, char* username) {
    int index = self->customerList->find(self->customerList, username);
    if(index >= 0) {
        printf("User %s successfully deleted!\n", username);
        self->customerList->remove(self->customerList, index);
        return;
    }
    index = self->employeeList->find(self->employeeList, username);
    if(index >= 0) {
        printf("User %s successfully deleted!\n", username);
        self->employeeList->remove(self->employeeList, index);
        return;
    }
    userDoesNutExist();
}

void Core__free(struct Core* self) {
    self->employeeList->free(self->employeeList);
    self->customerList->free(self->customerList);
    free(self);
}

int Core__checkExistCustomer(struct Core* self, char* username, int exeption) {
    if(self->customerList->find(self->customerList, username) >= 0) {
        if(exeption) customerExist();
        return True;
    }
    return False;
}
int Core__checkExistEmployee(struct Core* self, char* username, int exeption) {
    if(self->employeeList->find(self->employeeList, username) >= 0) {
        if(exeption) employeeExist();
        return True;
    }
    return False;
}

int Core__checkExist(struct Core* self, char* username, int exeption) {
    return(Core__checkExistCustomer(self, username, exeption) || Core__checkExistEmployee(self, username, exeption));
}

void Core__calculate_profit(struct Core* self) {
    self->customerList->calculate_profit(self->customerList);
}

void Core__show_credit(struct Core* self, char* username) {
    if(!Core__checkExistCustomer(self, username, False)) {
        customerNotExist();
        return;
    }
    printf("%ld\n",
    self->customerList->list[self->customerList->find(self->customerList, username)]->quantity);
}

void Core__increase(struct Core* self, int index, long int quantity) {
    self->customerList->list[index]->increase(self->customerList->list[index], quantity);
    
}
int Core__decrease(struct Core* self, int index, long int quantity) {
    return self->customerList->list[index]->decrease(self->customerList->list[index], quantity);    
}

int Core__transfer(struct Core* self, int index, long int quantity, char* target) {
    if(!Core__checkExistCustomer(self, target, False)) {
        customerNotExist();
        return False;
    }
    if(!self->decrease(self, index, quantity))
        return False;
    self->increase(self, self->customerList->find(self->customerList, target), quantity);
    return True;
}

void Core__showWorth(struct Core* self) {
    printf("%ld\n", self->customerList->sumQuantity(self->customerList));
}

void Core__reportTurnOver(struct Core* self, char* username) {
    int index = self->customerList->find(self->customerList, username);
    self->customerList->list[index]->reportTurnOver(self->customerList->list[index]);
}

void Core__saveData(struct Core* self, char* PATH) {
    FILE* file = fopen(PATH,"w");
    self->employeeList->saveInFile(self->employeeList, file);
    self->customerList->saveInFile(self->customerList, file);
    fclose(file);
}

void Core__loadData(struct Core* self, char* PATH) {
    FILE* file = fopen(PATH,"r");
    if (file == NULL) return;
    self->employeeList->loadOfFile(self->employeeList, file);
    self->customerList->loadOfFile(self->customerList, file);
    fclose(file);
}


struct Core* newCore() {
    struct Core* c = (struct Core*)malloc(sizeof(struct Core));
    c->employeeList = newEmployeeVector();
    c->customerList = newCustomerVector();
    c->signup_employee = Core__signup_employee;
    c->signup_customer = Core__signup_customer;
    c->login_employee = Core__login_employee;
    c->employee_list = Core__employee_list;
    c->login_customer = Core__login_customer;
    c->customer_list = Core__customer_list;
    c->delete_user = Core__delete_user;
    c->free = Core__free;
    c->checkExist = Core__checkExist;
    c->calculate_profit = Core__calculate_profit;
    c->show_credit = Core__show_credit;
    c->increase = Core__increase;
    c->decrease = Core__decrease;
    c->transfer = Core__transfer;
    c->showWorth = Core__showWorth;
    c->reportTurnOver = Core__reportTurnOver;
    c->save_data = Core__saveData;
    c->load_data = Core__loadData;
    return c;
}