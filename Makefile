cm = gcc # -fsanitize=address -fno-omit-frame-pointer

all: build a.out

build:
	mkdir -p build
a.out: code/main.c build/Interface.o build/Core.o build/myString.o build/EmployeeVector.o build/Employee.o build/Exeption.o build/CustomerVector.o build/Customer.o
	$(cm) code/main.c build/Interface.o build/Core.o build/myString.o build/EmployeeVector.o build/Employee.o build/Exeption.o build/CustomerVector.o build/Customer.o
	rm -rf build/
build/Interface.o: code/Interface.c code/Interface.h
	$(cm) -c code/Interface.c -o build/Interface.o
build/Core.o: code/Core.c code/Core.h
	$(cm) -c code/Core.c -o build/Core.o
build/myString.o: code/myString.c code/myString.h
	$(cm) -c code/myString.c -o build/myString.o
build/EmployeeVector.o: code/EmployeeVector.c code/EmployeeVector.h
	$(cm) -c code/EmployeeVector.c -o build/EmployeeVector.o
build/Employee.o: code/Employee.c code/Employee.h
	$(cm) -c code/Employee.c -o build/Employee.o
build/Exeption.o: code/Exeption.c code/Exeption.h
	$(cm) -c code/Exeption.c -o build/Exeption.o
build/CustomerVector.o: code/CustomerVector.c code/CustomerVector.h
	$(cm) -c code/CustomerVector.c -o build/CustomerVector.o
build/Customer.o: code/Customer.c code/Customer.h
	$(cm) -c code/Customer.c -o build/Customer.o
clean:
	rm -rf a.out data.txt